# SkyFramework
A PHP based framework for web applications

ToDo:
  - write documentation/wiki
  - extended ORM system
  - extended exception system
  - implement form modules (& create smarty plugin {form}, {input type=""}, ...)
  - implement regex helper
  - implement upload helper
  - implement cronjob helper
  - implement cache engine
  - implement page utils (e.g. sitemap, ...)
