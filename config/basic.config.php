<?php
/**
 * Project: SkyFramework2
 * File: basic.config.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.02.2016 - 11:18
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
$__config = array(

    'page' => array(

        'title' => 'SkyFramework',
        'description' => '',
        'keywords' => ''

    ),

    'defaults' => array(

        'designID' => 'skyxocuts3'

    ),

    // Advanced settings!
    '3rdParty' => array(

        // special format
        'template' => array(

            // Path can be FRAMEWORK or PUBLIC
            'path' => 'FRAMEWORK',
            'directory' => 'smarty',
            'mainClassFile' => 'Smarty.class.php',
            'mainClass' => 'Smarty'

        ),

        'logger' => array(

            'namespace' => 'SkyFramework\\logger\\Logger',

        ),

        'session' => array(

            'namespace' => 'SkyFramework\\session\\Session',

        ),

        'cache' => array(

            'namespace' => 'SkyFramework\\cache\\CacheHandler',

        ),

        'event' => array(

            'namespace' => 'SkyFramework\\event\\EventHandler',

        ),

    )

);