<?php
/**
 * Project: SkyFramework2
 * File: database.config.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.02.2016 - 11:05
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */

// TODO: Multi-Database access
$database = array(

    /**
     * allowed database types:
     * - mysql
     */
    'type' => 'mysql',

    'host' => 'localhost',
    'port' => 3306,
    'username' => 'root',
    'password' => 'till123',
    'database' => 'sfw2'

);