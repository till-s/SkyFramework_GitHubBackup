<?php
/**
 * Project: SkyFramework2
 * File: routes.config.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.02.2016 - 16:43
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */

// NOTICE: Will not be readed by framework!!
$__routes = array(

    'home' => array(
        'method' => 'GET',
        'schema' => '/',
        'target' => 'Index'
    ),

    'default' => array(
        'method' => 'GET',
        'schema' => '/[a:controller]/[i:id]?',
        'target' => ''
    ),

    'ajax' => array(
        'method' => 'POST',
        'schema' => '/[a:controller]/[a:action]/[a:params]',
        'target' => ''
    )

);