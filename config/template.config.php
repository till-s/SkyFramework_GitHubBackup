<?php
/**
 * Project: SkyFramework2
 * File: template.config.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.02.2016 - 11:18
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */

$__template = array(

    'defaultTemplateDir' => 'templates/',
    'compileDir' => 'templates/compiled/'

);