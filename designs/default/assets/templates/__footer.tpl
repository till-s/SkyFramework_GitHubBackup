<footer class="footer-wrapper">
    <div class="container">
        {include file="__footer_menu.tpl"}

        <span class="copyright">Copyright &copy; {$smarty.now|date_format:"%Y"} - {$__pageData.title}</span>
        <span class="copyright">Design by <strong>SkyxoCuts</strong></span>
    </div>
</footer>

<!-- JS Frameworks -->
<script src="{$__design->getRelativeDir()}assets/js/jquery-2.2.0.min.js"></script>
<script src="{$__design->getRelativeDir()}bootstrap/js/bootstrap.min.js"></script>
<script src="{$__design->getRelativeDir()}assets/js/main.js"></script>