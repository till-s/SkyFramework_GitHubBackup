<div class="row footer-menu">
    <div class="col-md-4 col-sm-6 footer-menu-box" data-position="left">
        <h4 class="footer-menu-box-headline">Social Media</h4>
        <ul class="list-unstyled footer-menu-box-list">
            <li><a href="#">YouTube</a></li>
            <li><a href="#">Facebook</a></li>
            <li><a href="#">Twitter</a></li>
        </ul>
    </div>
    <div class="col-md-4 hidden-xs hidden-sm footer-logo-box">
        <span class="footer-logo">{$__pageData.title}</span>
    </div>
    <div class="col-md-4 col-sm-6 footer-menu-box" data-position="right">
        <h4 class="footer-menu-box-headline">Rechtliches</h4>
        <ul class="list-unstyled footer-menu-box-list">
            <li><a href="#">Impressum</a></li>
            <li><a href="#">Datenschutzerklärung</a></li>
        </ul>
    </div>
</div>