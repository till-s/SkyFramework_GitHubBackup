<head>
    <meta charset="UTF-8">
    <title>{$__siteTitle} - {$__pageData.title}</title>

    <meta name="title" content="{$__siteTitle} - {$__pageData.title}" />
    <meta name="description" content="{$__pageData.description}" />
    <meta name="keywords" content="{$__pageData.keywords}" />

    <link rel="stylesheet" href="{$__design->getRelativeDir()}bootstrap/css/bootstrap.min.css" type="text/css" />

    {foreach from=$__design->getCSSFiles() item=css}
    <link rel="stylesheet" href="{$__design->getRelativeDir()}assets/css/{$css}" type="text/css" />
    {/foreach}
</head>