<header class="header-wrapper">
    <div class="logo-wrapper hidden-sm hidden-xs">
        <div class="logo">{$__pageData.title}</div>
    </div>

    {include file="__navigation.tpl"}

    <div class="header-content-wrapper">

    </div>

    <!--<div class="countdown-wrapper" id="countdownObject">
        <div class="container">

            <div class="outer-line hidden-sm hidden-xs" data-position="left"></div>
            <div class="outer-line hidden-sm hidden-xs" data-position="right"></div>

            <div class="circle-wrapper" id="countdownWrapper">
                <div class="circle-container">
                    <div class="default-content" id="countdownDefaultContent">
                        <div class="upper-text">
                            Neujahr <strong>2016</strong>
                            <br />in
                        </div>
                        <div class="countdown-container">
                            <span class="countdown-nr" id="countdownValue" data-type="day">00</span>
                            <span class="countdown-divider">:</span>
                            <span class="countdown-nr" id="countdownValue" data-type="hour">00</span>
                            <span class="countdown-divider">:</span>
                            <span class="countdown-nr" id="countdownValue" data-type="min">00</span>

                            <div class="hidden" id="countdownSeconds">
                                <span class="countdown-divider">:</span>
                                <span class="countdown-nr" id="countdownValue" data-type="sec">60</span>
                            </div>
                        </div>
                        <div class="countdown-units">
                            <span class="countdown-unit" data-type="day">Tage</span>
                            <span class="countdown-unit" data-type="hour">Std.</span>
                            <span class="countdown-unit" data-type="min">Min.</span>
                        </div>
                        <div class="bottom-text">
                        </div>
                    </div>
                    <div class="special-content hidden" id="countdownSpecialContent">10</div>
                </div>
            </div>

        </div>
    </div>-->

</header><!-- /header -->