<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation" aria-expanded="false">
                <span class="sr-only">Navigation �ffnen</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-lg hidden-md" href="#">CENTURYLAB</a>
        </div>

        <div class="collapse nav navbar-collapse" id="navigation">
            <ul class="nav navbar-nav">
                <li><a href="index.html">Home</a></li>
                <li><a href="board.html">Forum</a></li>
                <li><a href="team.html">Das Team</a></li>
                <li><a href="network.html">Netzwerk</a></li>
            </ul>
        </div>
    </div>
</nav>