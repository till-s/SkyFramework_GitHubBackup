<!-- jQuery -->
<script src="{$__design->getRelativeDir()}assets/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{$__design->getRelativeDir()}bootstrap/js/bootstrap.min.js"></script>

<!-- Scrolling Nav JavaScript -->
<script src="{$__design->getRelativeDir()}assets/js/jquery.easing.min.js"></script>
<script src="{$__design->getRelativeDir()}assets/js/main.js"></script>