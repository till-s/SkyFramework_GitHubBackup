<body id="page-top" data-spy="scroll" data-target="#navigation">

    <header class="header-wrapper">
        <!-- Navigation -->
        <nav class="navigaton-wrapper navbar navbar-default navbar-fixed-top" role="navigation" id="navigation">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-collapse">
                        <span class="sr-only">Men�</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="#page-top">{$__pageData.title}</a>
                </div>

                <div class="collapse navbar-collapse navbar-right" id="navigation-collapse">
                    <ul class="nav navbar-nav">
                        <li class="hidden"><a class="page-scroll" href="#page-top"></a></li>
                        <li><a class="page-scroll" href="#about">About</a></li>
                        <li><a class="page-scroll" href="#services">Services</a></li>
                        <li><a class="page-scroll" href="#contact">Contact</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
        </nav>
    </header>