<?php
namespace SkyFramework\app;

use SkyFramework\singleton\SingletonObject;

/**
 * Project: SkyFramework2
 * File: AppManager.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     29.02.2016 - 20:11
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class AppManager extends SingletonObject
{

    private $apps = array();

    public function init()
    {
        foreach ($this->apps as $app) {

        }
    }


}