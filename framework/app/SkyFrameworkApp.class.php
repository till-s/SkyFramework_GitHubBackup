<?php
namespace SkyFramework\app;

use SkyFramework\singleton\SingletonObject;

/**
 * Project: SkyFramework2
 * File: SkyFrameworkApp.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     29.02.2016 - 20:08
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
abstract class SkyFrameworkApp extends SingletonObject
{


}