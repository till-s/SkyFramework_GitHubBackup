<?php
namespace SkyFramework\cache;

/**
 * Project: SkyFramework2
 * File: CacheEngine.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     25.02.2016 - 19:37
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
interface CacheEngine
{
}