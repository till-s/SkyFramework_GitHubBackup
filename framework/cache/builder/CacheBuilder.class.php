<?php
namespace SkyFramework\cache\builder;

use SkyFramework\exception\CacheException;
use SkyFramework\util\StringUtil;

/**
 * Project: SkyFramework2
 * File: CacheBuilder.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     13.02.2016 - 11:46
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
abstract class CacheBuilder
{

    protected $cache = array();

    protected $filename = '';
    protected $maxLivetime = 0;

    public abstract function buildCache(array $parameters);

    public final function getData(array $parameters = array(), $cacheIndex = '')
    {
        if (empty($this->filename)) {
            $reflectionClass = new \ReflectionClass(__CLASS__);

            $className = strtolower($reflectionClass->getShortName());
            $className = str_replace('CacheBuilder', '', array_pop($className));

            $this->filename = sha1($className . md5($this->maxLivetime)) . '.cache.' . $className . '.php';
        }

        $filePath = ROOT . 'cache/' . $this->filename;

        if (!file_exists($this->filename)) {
            $this->cache = $this->buildCache($parameters);
        } else if (time() - filemtime($filePath) > $this->maxLivetime) {
            $this->cache = $this->buildCache($parameters);
        } else {
            $this->cache = unserialize(file_get_contents($filePath));
        }

        if (!empty($cacheIndex) && isset($this->cache[$cacheIndex])) {
            return $this->cache[$cacheIndex];
        }

        return $this->cache;
    }

}