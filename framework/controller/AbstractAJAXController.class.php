<?php
namespace SkyFramework\controller;

use SkyFramework\controller\AbstractController;
use SkyFramework\core\SkyFramework;
use SkyFramework\exception\SystemException;
use SkyFramework\util\StringUtil;
use SkyFramework\util\Utils;

/**
 * Project: SkyFramework2
 * File: AbstractAJAXController.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     26.03.2016 - 16:01
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
abstract class AbstractAJAXController extends AbstractController
{

    public abstract function readParameters();

    public abstract function readData();

    /**
     * @throws SystemException
     */
    private function checkReferrer()
    {
        if (!isset($_SERVER['HTTP_REFERER']) || !preg_match('#^' . Utils::getURLPattern() . '#', $_SERVER['HTTP_REFERER'])) {
            throw new SystemException('AJAX Request is invalid!');
        }
    }

    /**
     * @throws SystemException
     */
    private function checkSecurityToken()
    {
        if (!isset($_REQUEST['__t']) || !SkyFramework::getSession()->checkSecurityToken($_REQUEST['__t'])) {
            throw new SystemException('Sended AJAX token is invalid!');
        }

        SkyFramework::getSession()->resetSecurityToken();
    }

    /**
     * @param array $data
     * @throws SystemException
     */
    protected function sendResponse(array $data)
    {
        if (!is_array($data)) {
            throw new SystemException('Response for AJAX must be an array!');
        }

        header('Content-type: application/json');
        echo json_encode($data);
        exit;
    }

    /**
     * @throws SystemException
     */
    public function execute()
    {
        // Security checks
        $this->checkReferrer();
        $this->checkSecurityToken();

        // Read parameters and other data
        $this->readParameters();
        $this->readData();
    }
}