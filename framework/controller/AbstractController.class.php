<?php
namespace SkyFramework\controller;

use SkyFramework\controller\view\AbstractView;
use SkyFramework\core\SkyFramework;

/**
 * Project: SkyFramework2
 * File: AbstractController.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     13.02.2016 - 16:53
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
abstract class AbstractController
{

    /**
     * @var null|AbstractView
     */
    protected $view = null;

    /**
     * Run controller
     */
    public function __run()
    {
        $this->execute();
    }

    /**
     * @param AbstractView $view
     */
    public function setView(AbstractView $view)
    {
        $this->view = $view;
    }

    /**
     * Execute controller
     *
     * @return mixed
     */
    public abstract function execute();

}