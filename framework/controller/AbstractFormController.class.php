<?php
namespace SkyFramework\controller;

use SkyFramework\core\SkyFramework;
use SkyFramework\exception\html\HTMLFormException;

/**
 * Project: SkyFramework2
 * File: AbstractFormController.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 19:57
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
abstract class AbstractFormController extends AbstractPageController
{

    /**
     * Read parameters of form
     */
    public function readFormParameters()
    {
    }

    /**
     * Save form data
     */
    public function save()
    {
    }

    /**
     * Called if there is no exception thrown
     */
    public function finishForm()
    {
    }


    /**
     * Listener for submitted form
     */
    public function readData()
    {
        if (!empty($_POST) || !empty($_FILES)) {
            $this->submit();
        }
    }

    /**
     * Validate form
     */
    private function validate()
    {
        if (!isset($_POST['__t']) || empty($_POST['__t'])) {
            throw new HTMLFormException('Invalid form token');
        }

        if (!SkyFramework::getSession()->checkSecurityToken($_POST['__t'])) {
            throw new HTMLFormException('Invalid form token');
        }

        SkyFramework::getSession()->resetSecurityToken();
    }

    /**
     * Submit form
     */
    private function submit()
    {
        try {
            $this->validate();

            $this->readFormParameters();
            $this->save();
        } catch (HTMLFormException $e) {

        }

        $this->finishForm();
    }

}