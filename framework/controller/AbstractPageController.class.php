<?php
namespace SkyFramework\controller;

use SkyFramework\controller\view\AbstractDesignView;
use SkyFramework\controller\view\AbstractTemplateView;
use SkyFramework\core\SkyFramework;
use SkyFramework\exception\SystemException;

/**
 * Project: SkyFramework2
 * File: AbstractPageController.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     13.02.2016 - 16:52
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
abstract class AbstractPageController extends AbstractController
{

    /**
     * @var null|AbstractDesignView|AbstractTemplateView
     */
    protected $view = null;

    /**
     * @var string
     */
    protected $siteTitle = '';

    /**
     *
     */
    public function __construct()
    {

    }

    public function readParameters()
    {
    }

    public function readData()
    {
    }

    public function assignVariables()
    {
    }

    private function assignDefaultVariables()
    {
        SkyFramework::getTemplate()->assign('__siteTitle', $this->siteTitle);
    }

    public final function execute() {
        if ($this->view === null) {
            throw new SystemException('There is no View for Controller "' . __CLASS__ . '"!');
        }

        $this->readParameters();
        $this->readData();
        $this->assignVariables();
        $this->assignDefaultVariables();

        $this->view->output();
    }

}