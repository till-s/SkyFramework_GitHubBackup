<?php
namespace SkyFramework\controller\view;

use SkyFramework\core\SkyFramework;
use SkyFramework\request\RequestHandler;

/**
 * Project: SkyFramework2
 * File: AbstractDesignView.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 15:49
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class AbstractDesignView extends AbstractTemplateView
{

    /**
     * @return mixed
     */
    public final function setTemplateDirectory()
    {
        $designTemplatePath = SkyFramework::getDesignManager()->getDefaultDesign()->getTemplatePath();
        $viewTemplatePath = dirname(RequestHandler::getInstance()->getActiveControllerInfo()->getControllerPath()) . '/' . $this->templateDir;

        SkyFramework::getTemplate()->setTemplateDir(array(
            $designTemplatePath,
            $viewTemplatePath
        ));
    }

    /**
     * @return mixed
     */
    public function assignVariables()
    {
        SkyFramework::getTemplate()->assign('__design', SkyFramework::getDesignManager()->getDefaultDesign());
    }

}