<?php
namespace SkyFramework\controller\view;

use SkyFramework\controller\AbstractPageController;
use SkyFramework\core\SkyFramework;

/**
 * Project: SkyFramework2
 * File: AbstractTemplateView.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 15:05
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
abstract class AbstractTemplateView extends AbstractView
{

    /**
     * @var string
     */
    protected $template = '';

    /**
     * @var string
     */
    protected $templateDir = '';

    /**
     * @var array
     */
    protected $variables = array();

    /**
     * @return mixed
     */
    protected abstract function setTemplateDirectory();

    /**
     * @return mixed
     */
    protected abstract function assignVariables();

    /**
     * Output the view
     */
    public function output()
    {
        $this->setTemplateDirectory();
        $this->assignVariables();

        SkyFramework::getTemplate()->display($this->template);
    }

}