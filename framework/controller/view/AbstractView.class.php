<?php
namespace SkyFramework\controller\view;
use SkyFramework\controller\AbstractController;

/**
 * Project: SkyFramework2
 * File: AbstractView.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 14:51
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
abstract class AbstractView
{

    /**
     * @var AbstractController
     */
    protected $controller;

    /**
     * AbstractView constructor.
     *
     * @param AbstractController $controller
     */
    public function __construct(AbstractController $controller)
    {
        $this->controller = $controller;
    }

    public abstract function output();

}