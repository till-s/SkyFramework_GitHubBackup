<?php
namespace SkyFramework\core;

use SkyFramework\database\driver\Database;
use SkyFramework\database\driver\mysql\MySQLDatabase;
use SkyFramework\design\DesignManager;
use SkyFramework\exception\IOException;
use SkyFramework\exception\PrintableException;
use SkyFramework\exception\SystemException;
use SkyFramework\logger\Logger;
use SkyFramework\logger\LoggerEngine;
use SkyFramework\request\RequestHandler;
use SkyFramework\session\Session;
use SkyFramework\session\SessionEngine;
use SkyFramework\template\TemplateEngine;
use SkyFramework\util\ClassUtil;

/**
 * Project: SkyFramework2
 * File: SkyFramework.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     11.02.2016 - 17:55
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
final class SkyFramework
{

    /**
     * @var MySQLDatabase
     */
    private static $databaseObj = null;

    /**
     * @var SessionEngine
     */
    private static $sessionObj = null;

    /**
     * @var TemplateEngine
     */
    private static $templateObj = null;

    /**
     * @var array
     */
    private $__config = array();

    /**
     * SkyFramework constructor.
     */
    public function __construct()
    {
        $this->loadConfig();
        $this->registerHandler();
        $this->defineConstants();

        $this->initEvents();
        $this->initDatabase();
        // $this->initCache();
        $this->initSession();
        $this->initTemplate();
        $this->initRequest();
    }

    private final function loadConfig()
    {
        $__config = array();
        require(CF_ROOT . 'basic.config.php');
        $this->__config = $__config;
    }

    /**
     *
     */
    private final function registerHandler()
    {
        spl_autoload_register(array('SkyFramework\core\SkyFramework', '__autoload'));
        set_exception_handler(array('SkyFramework\core\SkyFramework', '__exceptionHandler'));
    }

    /**
     *
     */
    private final function defineConstants()
    {

    }

    /**
     *
     */
    private function initEvents()
    {
        $classNamespace = $this->__config['3rdParty']['event']['namespace'];
        if (!class_exists($classNamespace)) {
            throw new SystemException('Could not find event engine "' . $classNamespace . '"');
        }

        if (!ClassUtil::isInstanceOf($classNamespace, 'SkyFramework\\event\\EventEngine')) {
            throw new SystemException('Event class "' . $classNamespace . '" is not an instance of "SkyFramework\\event\\EventEngine"!');
        }
    }

    /**
     * @return Database
     */
    public static function getDatabase()
    {
        return self::$databaseObj;
    }

    /**
     *
     */
    private function initDatabase()
    {
        $database = array();
        require_once(CF_ROOT . 'database.config.php');

        $className = '';
        switch ($database['type']) {
            case 'mysql':
                $className = '\\SkyFramework\\database\\driver\\mysql\\MySQLDatabase';
                break;
        }

        if (!class_exists($className)) {
            throw new SystemException('Could not find database class "' . $className . '"');
        }

        self::$databaseObj = new $className($database['host'], (int)$database['port'], $database['username'], $database['password'], $database['database']);
    }

    /**
     * @return RequestHandler
     * @throws SystemException
     */
    public static function getRequest()
    {
        return RequestHandler::getInstance();
    }

    /**
     * Handle the current request
     */
    private function initRequest()
    {
        self::getRequest()->handleRequest();
    }

    /**
     * @return null|Session
     */
    public static function getSession()
    {
        return self::$sessionObj;
    }

    /**
     * @throws SystemException
     */
    private function initSession()
    {
        $classNamespace = $this->__config['3rdParty']['session']['namespace'];
        if (!class_exists($classNamespace)) {
            throw new SystemException('Could not find session engine "' . $classNamespace . '"');
        }

        if (!ClassUtil::isInstanceOf($classNamespace, 'SkyFramework\\session\\SessionEngine')) {
            throw new SystemException('Session class "' . $classNamespace . '" is not an instance of "SkyFramework\\session\\SessionEngine"!');
        }

        self::$sessionObj = new $classNamespace();
    }

    /**
     * @return DesignManager
     * @throws SystemException
     */
    public static function getDesignManager() {
        return DesignManager::getInstance();
    }

    /**
     * @return null|TemplateEngine
     */
    public static function getTemplate() {
        return self::$templateObj;
    }

    private function initTemplate() {
        $__template = array();

        require(CF_ROOT .'template.config.php');

        $pathType = $this->__config['3rdParty']['template']['path'];
        $pathRoot = '';
        switch (strtolower($pathType)) {
            case 'framework':
                $pathRoot = FW_ROOT;
                break;
            case 'public':
                $pathRoot = PB_ROOT;
                break;
            default:
                // fallback
                $pathRoot = FW_ROOT;
        }

        $classPath = realpath($pathRoot . 'template/' . $this->__config['3rdParty']['template']['directory'] . '/' . $this->__config['3rdParty']['template']['mainClassFile']);
        $className = '\\' . $this->__config['3rdParty']['template']['mainClass'];

        /*if (!FileUtil::isValidPath($classPath)) {
            throw new IOException('Path for 3rdParty Template Engine is not valid "'. $classPath .'"!');
        }
        else */
        if (!file_exists($classPath)) {
            throw new IOException('Cannot find 3rdParty Template Engine mainclass "' . $classPath . '"!');
        } else if (!ClassUtil::isInstanceOf($className, '\\SkyFramework\\template\\TemplateEngine', $classPath)) {
            throw new IOException('Template Engine "' . $className . '" does not implement SkyFramework\\template\\TemplateEngine');
        }

        if (!defined('SMARTY_RESOURCE_CHAR_SET')) {
            define('SMARTY_RESOURCE_CHAR_SET', 'UTF-8');
        }

        require_once($classPath);

        self::$templateObj = new $className;

        self::getTemplate()->setCompileDir(PB_ROOT . $__template['compileDir']);

        self::getTemplate()->assign('__core', $this);
        self::getTemplate()->assign('__pageData', $this->__config['page']);
    }

    /**
     * @param $className
     * @throws SystemException
     */
    public static final function __autoload($className)
    {
        $namespaces = explode('\\', $className);
        if (count($namespaces) > 1) {
            if ($namespaces[0] == 'SkyFramework') {
                $namespaces[0] = '';
            }

            $regularClassPath = substr(implode('/', $namespaces), 1);

            $classPaths = array();
            $classPaths['framework'] = FW_ROOT . $regularClassPath . '.class.php';
            $classPaths['framework1'] = FW_ROOT . $regularClassPath . '.php';
            $classPaths['public'] = PB_ROOT . $regularClassPath . '.class.php';
            $classPaths['public1'] = PB_ROOT . $regularClassPath . '.php';

            $importClassPath = '';
            $found = false;
            foreach ($classPaths as $classPath) {
                if (file_exists($classPath)) {
                    $found = true;
                    $importClassPath = $classPath;
                    break;
                }
            }

            #echo $regularClassPath . ' - '. $importClassPath .'<br />';

            if ($found) {
                require_once($importClassPath);
            } else {
                throw new SystemException('Class "' . $className . '" not found in "' . $classPath . '"!');
            }
        }
    }

    /**
     * @param \Exception $e
     */
    public static final function __exceptionHandler(\Exception $e)
    {
        $reflectionClass = new \ReflectionClass(get_class($e));
        Logger::getInstance()->log('(' . $reflectionClass->getShortName() . ') ' . $e->getMessage(), LoggerEngine::FATAL);

        if ($e instanceof PrintableException) {
            $e->show();
        } else {
            echo str_replace("\n", '<br />', $e);
        }
    }

}