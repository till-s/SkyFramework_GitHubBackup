<?php
namespace SkyFramework\database\driver;
use SkyFramework\database\statement\PreparedStatement;

/**
 * Project: SkyFramework2
 * File: Database.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     11.02.2016 - 18:13
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
abstract class Database
{

    protected $host = "";
    protected $port = 3306;
    protected $user = "";
    protected $password = "";
    protected $database = "";

    /**
     * @var \PDO
     */
    protected $connection = null;
    protected $queryCount = 0;

    /**
     * Database constructor.
     * @param string $host
     * @param int $port
     * @param string $user
     * @param string $password
     * @param string $database
     */
    public function __construct($host, $port, $user, $password, $database)
    {
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
        $this->database = $database;

        $this->connect();
    }

    /**
     * @return mixed
     */
    public abstract function connect();

    /**
     * @param $query
     * @return PreparedStatement
     */
    public abstract function prepare($query);

    public function getInsertID($table = '') {
        return $this->connection->lastInsertId();
    }

    /**
     * @return int
     */
    public function getQueryCount()
    {
        return $this->queryCount;
    }

}