<?php
namespace SkyFramework\database\driver\mysql;
use SkyFramework\database\driver\Database;
use SkyFramework\database\statement\PreparedStatement;
use SkyFramework\exception\DatabaseException;

/**
 * Project: SkyFramework2
 * File: MySQLDatabase.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     11.02.2016 - 19:26
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class MySQLDatabase extends Database
{

    public function connect()
    {
        try {
            $this->connection = new \PDO('mysql:host=' . $this->host . ';dbname=' . $this->database . '', $this->user, $this->password);
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            throw new DatabaseException($e->getMessage());
        }
    }

    /**
     * @see Database::prepare
     * @return PreparedStatement
     */
    public function prepare($query)
    {
        try {
            $stmt = $this->connection->prepare($query);
            $this->queryCount++;
            return new PreparedStatement($this, $stmt, $query);
        } catch(\PDOException $e) {
            throw new DatabaseException($e->getMessage());
        }
    }

}