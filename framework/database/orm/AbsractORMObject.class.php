<?php
namespace SkyFramework\database\orm;
use SkyFramework\core\SkyFramework;
use SkyFramework\exception\ORMException;

/**
 * Project: SkyFramework2
 * File: AbsractORMObject.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     12.02.2016 - 15:25
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
abstract class AbsractORMObject
{

    private $modifiedFields = array();

    protected static $databaseTable = "";
    protected static $databaseTablePrimaryKey = "";
    protected static $databaseTablePrimaryKeyValue = null;

    protected $loadData;
    protected $loadType;

    const LOAD_EMPTY = 0;
    const LOAD_PRIMARYKEY = 1;
    const LOAD_ARRAY = 2;

    /**
     * AbsractORMObject constructor.
     */
    public function __construct($loadData = null, $loadType = self::LOAD_EMPTY)
    {
        $this->loadData = $loadData;
        $this->loadType = $loadType;

        $this->handleData();
    }

    private final function handleData() {
        switch ($this->loadType) {
            case self::LOAD_EMPTY:

                break;
            case self::LOAD_ARRAY:
                $this->handleDataFromArray();
                break;
            case self::LOAD_PRIMARYKEY:
                $this->handleDataFromDatabase();
                break;
        }
    }

    private final function handleDataFromArray() {
        if (!is_array($this->loadData)) {
            throw new ORMException("Commited data is not an array");
        }

        foreach ($this->loadData as $key => $value) {
            $this->{$key} = $value;
        }
    }

    private final function handleDataFromDatabase() {
        $sql = sprintf("SELECT * FROM `%s` WHERE `%s` = ?", self::$databaseTable, self::$databaseTablePrimaryKey);
        $stmt = SkyFramework::getDatabase()->prepare($sql);
        $stmt->execute(array(self::$databaseTablePrimaryKeyValue));
        $row = $stmt->fetchArray();
        if ($row === false || $row === null) {
            throw new ORMException("Cannot collect fetched data from database");
        }

        foreach ($row as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function set($tableField, $newValue) {
        if ($this->{$tableField} == $newValue) return;

        array_push($this->modifiedFields, $tableField);

        $this->{$tableField} = $newValue;
    }

    public function save() {
        if (empty(self::$databaseTable)) {
            throw new ORMException("Table for database is not defined");
        }
        else if (empty(self::$databaseTablePrimaryKey)) {
            throw new ORMException("Primary key of table for database is not defined");
        } else if (empty(self::$databaseTablePrimaryKeyValue) && $this->loadType == 1) {
            throw new ORMException("Value of primary key is not defined");
        }

        switch ($this->loadType) {
            case self::LOAD_EMPTY:
            case self::LOAD_ARRAY:
            if (!isset(self::$databaseTable) || self::$databaseTable === null || !isset(self::$databaseTablePrimaryKey) || self::$databaseTablePrimaryKey === null) {
                throw new ORMException('Primary key or table is not set');
            }
                $this->loadType = self::LOAD_PRIMARYKEY;
                return $this->insert();
                break;
            case self::LOAD_PRIMARYKEY:
                $this->update();
                break;
        }
    }

    private function insert() {
        $columnNames = "";
        $valueKeys = "";
        $valueParameters = array();

        if ($this->loadType == 1) {
            if (empty($this->modifiedFields))
                $this->modifiedFields = array_keys($this->loadData);
            else
                array_push($this->modifiedFields, array_keys($this->loadData));
        }

        foreach ($this->modifiedFields as $key) {
            $columnNames .= ", " . $key;
            $valueKeys .= ",?";
            array_push($valueParameters, $this->{$key});
        }

        $columnNames = substr($columnNames, 1);
        $valueKeys = substr($valueKeys, 1);

        $sql = sprintf("INSERT INTO `%s` (%s) VALUES (%s)", self::$databaseTable, $columnNames, $valueKeys);
        $stmt = SkyFramework::getDatabase()->prepare($sql);
        $stmt->execute($valueParameters);

        return SkyFramework::getDatabase()->getInsertID();
    }

    private function update() {
        $columNames = "";
        $valueParameters = array();

        foreach ($this->modifiedFields as $key) {
            $value = $this->{$key};
            $columNames .= ", ". $key ." = ?";
            array_push($valueParameters, $value);
        }

        $columNames = substr($columNames, 1);
        array_push($valueParameters, self::$databaseTablePrimaryKeyValue);

        $sql = sprintf("UPDATE `%s` SET %s WHERE `%s`= ?", self::$databaseTable, $columNames, self::$databaseTablePrimaryKey);
        $stmt = SkyFramework::getDatabase()->prepare($sql);
        $stmt->execute($valueParameters);
    }

    /**
     * @param string $databaseTable
     */
    public function setDatabaseTable($databaseTable)
    {
        $this->databaseTable = $databaseTable;
    }

    /**
     * @param string $databaseTablePrimaryKey
     */
    public function setDatabaseTablePrimaryKey($databaseTablePrimaryKey)
    {
        $this->databaseTablePrimaryKey = $databaseTablePrimaryKey;
    }

    /**
     * @param null $databaseTablePrimaryKeyValue
     */
    public function setDatabaseTablePrimaryKeyValue($databaseTablePrimaryKeyValue)
    {
        $this->databaseTablePrimaryKeyValue = $databaseTablePrimaryKeyValue;
    }

}