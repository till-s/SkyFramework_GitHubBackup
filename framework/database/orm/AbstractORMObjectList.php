<?php
namespace SkyFramework\database\orm;

use SkyFramework\core\SkyFramework;
use SkyFramework\exception\ORMException;
use SkyFramework\util\ClassUtil;

/**
 * Project: SkyFramework2
 * File: AbstractORMObjectList.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 20:56
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
abstract class AbstractORMObjectList
{

    protected static $databaseTable = '';
    protected static $selects = array();
    protected static $joins = array();
    protected static $group = array();
    protected static $objectClassName = '';
    protected static $limit = -1;

    protected $objectList = array();

    public function __construct()
    {
        if (!(ClassUtil::isInstanceOf(self::$objectClassName, 'SkyFramework\database\orm\AbstractORMObject'))) {
            throw new ORMException('Object class "' . self::$objectClassName . '" is not an instance of "SkyFramework\database\orm\AbstractORMObject"!');
        }

        $this->buildQuery();
    }

    private function buildQuery()
    {
        $sql = 'SELECT ' . self::$databaseTable . '.*';

        if (!empty($this->selects)) {
            foreach ($this->selects as $select) {
                $sql .= ',' . $select . ' ';
            }
        }

        if (!empty($this->joins)) {
            foreach ($this->joins as $join => $data) {
                $on = $data['on'];
                $and = $data['and'];

                $sql .= ' LEFT JOIN ' . $join . ' ON (' . $on . ') ';

                if (!empty($and)) {
                    $this->handleAndOrJoin($sql, $join, $data);
                }
            }
        }

        // TOOD: implement self::$group

        if (self::$limit > 0) {
            $sql .= ' LIMIT ' . self::$limit;
        }

        $this->executeQuery($sql);
    }

    private function executeQuery($sql)
    {
        $stmt = SkyFramework::getDatabase()->prepare($sql);
        $stmt->execute();

        $this->handleResult($stmt->fetchAll());
    }

    private function handleResult($result)
    {
        foreach ($result as $array) {
            $this->objectList[] = new self::$objectClassName($result, AbsractORMObject::LOAD_ARRAY);
        }
    }

    private function handleAndOrJoin($sql, $table, $data)
    {
        $sql .= ' AND ' . $table . ' ON (' . $data . ') ';

        if (isset($data['and']) && !empty($data['and'])) {
            $this->handleAndOrJoin($sql, $table, $data['and']);
        }
    }

    /**
     * @return array
     */
    public function getObjectList()
    {
        return $this->objectList;
    }

}