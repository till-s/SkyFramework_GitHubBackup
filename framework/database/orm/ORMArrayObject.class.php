<?php
namespace SkyFramework\database\orm;

/**
 * Project: SkyFramework2
 * File: ORMArrayObject.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     12.02.2016 - 16:32
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class ORMArrayObject extends AbsractORMObject
{

    /**
     * ORMArrayObject constructor.
     * @param null $loadData
     * @param null $databaseTable
     * @param null $primaryKey
     */
    public function __construct($loadData, $databaseTable = null, $primaryKey = null)
    {
        if ($databaseTable !== null && $primaryKey !== null) {
            $this->databaseTable = $databaseTable;
            $this->databaseTablePrimaryKey = $primaryKey;
        }

        parent::__construct($loadData, self::LOAD_ARRAY);
    }

}