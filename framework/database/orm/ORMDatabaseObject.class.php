<?php
namespace SkyFramework\database\orm;

/**
 * Project: SkyFramework2
 * File: ORMDatabaseObject.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     12.02.2016 - 15:23
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class ORMDatabaseObject extends AbsractORMObject
{

    /**
     * ORMObject constructor.
     */
    public function __construct($databasesTable, $primaryKey, $primaryKeyValue)
    {
        $this->databaseTable = $databasesTable;
        $this->databaseTablePrimaryKey = $primaryKey;
        $this->databaseTablePrimaryKeyValue = $primaryKeyValue;

        parent::__construct(null, AbsractORMObject::LOAD_PRIMARYKEY);
    }

}