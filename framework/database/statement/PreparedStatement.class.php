<?php
namespace SkyFramework\database\statement;

use SkyFramework\database\driver\Database;
use SkyFramework\exception\DatabaseException;

/**
 * Project: SkyFramework2
 * File: PreparedStatement.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     11.02.2016 - 19:31
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class PreparedStatement
{

    private $database = null;
    private $statement = null;
    private $query = "";
    private $parameters = array();

    /**
     * PreparedStatement constructor.
     * @param null|Database $database
     * @param null|\PDOStatement $statement
     * @param string $query
     */
    public function __construct(Database $database, \PDOStatement $statement, $query)
    {
        $this->database = $database;
        $this->statement = $statement;
        $this->query = $query;
    }

    /**
     *
     *
     * @param array $parameters
     * @throws DatabaseException
     */
    public function execute(array $parameters = array()) {
        $this->parameters = $parameters;
        try {
            if (empty($this->parameters)) {
                $this->statement->execute();
            }
            else {
                $this->statement->execute($this->parameters);
            }
        } catch (\PDOException $e) {
            throw new DatabaseException($e->getMessage());
        }
    }

    /**
     *
     *
     * @param null $type
     * @return mixed
     */
    public function fetchArray($type = null) {
        if ($type === null) $type = \PDO::FETCH_ASSOC;
        return $this->statement->fetch($type);
    }

    public function fetchAll() {
        return $this->statement->fetchAll();
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

}