<?php
namespace SkyFramework\design;

use SkyFramework\event\EventHandler;
use SkyFramework\exception\SystemException;
use SkyFramework\io\Compressor;
use SkyFramework\io\File;
use SkyFramework\logger\Logger;
use SkyFramework\logger\LoggerEngine;
use SkyFramework\util\FileUtil;
use SkyFramework\util\StringUtil;
use SkyFramework\util\Utils;

/**
 * Project: SkyFramework2
 * File: Design.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.02.2016 - 17:45
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class Design
{

    private $designID;
    private $designName;
    private $author;
    private $version;
    private $description;

    private $directory;

    private $assetList = array();
    private $pathList = array();
    private $orderList = array();

    private $compressedFiles = array();


    /**
     * Design constructor.
     * @param $designID
     * @param $directory
     * @param $designConfigPath
     * @throws SystemException
     */
    public function __construct($designID, $directory, $designConfigPath)
    {
        $this->designID = $designID;
        $this->directory = $directory;

        if (!file_exists($designConfigPath)) {
            throw new SystemException('There is no design.info.ini in "' . $designConfigPath . '"!');
        }

        $designData = parse_ini_file($designConfigPath, true);

        $this->designName = $designData['name'];
        $this->author = $designData['author'];
        $this->version = $designData['version'];
        $this->description = $designData['description'];

        foreach ($designData['path'] as $type => $relativePath) {
            $path = $this->directory . $relativePath . (StringUtil::endsWith($relativePath, '/') ? '' : '/');

            if (!file_exists($path)) {
                Logger::getInstance()->log('Path in design with ID "' . $this->designID . '" have not directory "' . $path . '"! Continue ...', LoggerEngine::WARN);
                continue;
            }

            $this->assetList[$type] = $relativePath;
            $this->pathList[$type] = $path;
        }

        foreach ($designData['order'] as $type => $order) {
            $this->orderList[$type] = $order;
        }

        $this->compressDesign();
    }

    private function compressDesign()
    {
        $compressor = new DesignCompressor($this);
        $compressedPaths = $compressor->compress();

        $this->compressedFiles = $compressedPaths;
    }

    public function getAssetPath($type)
    {
        return isset($this->assetList[$type]) ? $this->assetList[$type] . (StringUtil::endsWith($this->assetList[$type], '/') ? '' : '/') : null;
    }

    public function getPath($type)
    {
        return isset($this->pathList[$type]) ? $this->pathList[$type] . (StringUtil::endsWith($this->pathList[$type], '/') ? '' : '/') : null;
    }

    public function getHTMLCompiledAssets($type, $compressed = true)
    {
        $imports = array();

        if ($compressed) {
            $imports[] = $this->compressedFiles[$type];
        } else {
            foreach ($this->getOrderList($type) as $file) {
                $imports[] = 'designs/' . $this->designID . '/' . $this->getAssetPath($type) . $file;
            }
        }

        $html = '';
        foreach ($imports as $import) {
            switch ($type) {
                case 'css':
                    $html .= '<link rel="stylesheet" type="text/css" href="' . $this->getRelativeURL() . $import . '" />' . "\n";
                    break;
                case 'js':
                    $html .= '<script type="application/javascript" src="' . $this->getRelativeURL() . $import . '"></script>' . "\n";
                    break;
            }
        }

        return $html;
    }

    public function getRelativeURL()
    {
        return Utils::getURL();
    }

    /**
     * @return mixed
     */
    public function getDesignID()
    {
        return $this->designID;
    }

    /**
     * @return mixed
     */
    public function getDesignName()
    {
        return $this->designName;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getDirectory()
    {
        return $this->directory . (StringUtil::endsWith($this->directory, '/') ? '' : '/');
    }

    /**
     * @return array
     */
    public function getPathList()
    {
        return $this->pathList;
    }

    /**
     * @return array
     */
    public function getAssetList()
    {
        return $this->assetList;
    }

    /**
     * @return mixed
     */
    public function getTemplatePath()
    {
        return $this->pathList['template'];
    }

    /**
     * @return array
     */
    public function getOrderList($type = null)
    {
        return ($type !== null ? $this->orderList[$type] : $this->orderList);
    }

}