<?php
namespace SkyFramework\design;

use SkyFramework\io\Compressor;
use SkyFramework\io\File;
use SkyFramework\util\FileUtil;
use SkyFramework\util\StringUtil;

/**
 * Project: SkyFramework2
 * File: DesignCompressor.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     20.03.2016 - 16:56
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class DesignCompressor
{

    private $design = null;

    public function __construct(Design $design)
    {
        $this->design = $design;
    }

    public function compress()
    {
        $relativeTempPath = 'tmp/design/';
        $tmpPath = PB_ROOT . $relativeTempPath;

        // TODO: wenn intervall von einer stunde nicht überschritten und tmp/design/... exestiert, dann hole alle dateien von dort und return diese

        $compressorAssets = array();

        $paths = array();
        foreach ($this->design->getOrderList() as $type => $fileList) {
            $compressorAssets[$type] = array();

            foreach ($fileList as $file) {
                $relativePath = $this->design->getAssetPath($type) . $file;
                $path = $this->design->getDirectory() . $relativePath;

                $compressorAssets[$type][] = $path;
            }
        }

        foreach ($compressorAssets as $type => $assets) {
            $compressor = new Compressor($assets);
            $compressedData = $compressor->compress();

            $relativeAssetPath = $this->design->getAssetPath($type) . sha1($this->design->getDesignID()) . '.' . $type;
            $tempAssetPath = $tmpPath . $relativeAssetPath;

            if (!file_exists(dirname($tempAssetPath))) {
                mkdir(dirname($tempAssetPath), 0777, true);
            }

            $file = new File($tempAssetPath);
            $file->write($compressedData);
            $file->close();

            $paths[$type] = 'public/' . $relativeTempPath . $relativeAssetPath;
        }

        $this->compressImages();
        $this->compressOtherAssets();

        return $paths;
    }

    // TODO: https://github.com/sabberworm/PHP-CSS-Parser   <- to hash image filenames and put that into compress css file

    public function compressImages()
    {
        $imagesPath = $this->design->getDirectory() . $this->design->getAssetPath('img');
        $destination = PB_ROOT . 'tmp/design/' . $this->design->getAssetPath('img');

        if (!file_exists($destination)) {
            mkdir($destination, 0777, true);
        }

        FileUtil::copyRecursive($imagesPath, $destination);
    }

    public function compressOtherAssets()
    {
        foreach ($this->design->getAssetList() as $type => $assetPath) {
            if ($type == 'css' || $type == 'js' || $type == 'img' || $type == 'templates') {
                continue;
            }

            $path = $this->design->getDirectory() . $assetPath . (StringUtil::endsWith($assetPath, '/') ? '' : '/');
            $destination = PB_ROOT . 'tmp/design/' . $assetPath . (StringUtil::endsWith($assetPath, '/') ? '' : '/');

            if (!file_exists($destination)) {
                mkdir($destination, 0777, true);
            }

            FileUtil::copyRecursive($path, $destination);
        }
    }

}