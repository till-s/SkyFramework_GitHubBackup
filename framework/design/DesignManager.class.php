<?php
namespace SkyFramework\design;
use SkyFramework\exception\SystemException;
use SkyFramework\logger\Logger;
use SkyFramework\logger\LoggerEngine;
use SkyFramework\singleton\SingletonObject;
use SkyFramework\util\StringUtil;

/**
 * Project: SkyFramework2
 * File: DesignManager.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.02.2016 - 17:32
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class DesignManager extends SingletonObject
{

    /**
     * @var string
     */
    private $designPath;

    /**
     * @var array
     */
    private $designs = array();

    /**
     * @var null|Design
     */
    private $defaultDesign = null;

    /**
     * @throws SystemException
     */
    protected function init()
    {
        $this->designPath = ROOT . 'designs/';

        $this->searchDesigns();
        $this->searchDefaultDesign();
    }

    /**
     * @throws SystemException
     */
    private function searchDesigns()
    {
        $designPathList = array_filter(glob($this->designPath . '*', GLOB_ONLYDIR), 'is_dir');

        foreach ($designPathList as $designPath) {
            $designPath = $designPath . (StringUtil::endsWith($designPath, '/') ? '' : '/');
            $designInfoData = $designPath . 'design.info.ini';
            $designID = basename($designPath);

            if (!file_exists($designInfoData)) {
                Logger::getInstance()->log('There is no design.info.ini file in design path "' . $designPath . '"! Continue ...', LoggerEngine::WARN);
                continue;
            }

            $this->designs[$designID] = array(
                'id' => $designID,
                'path' => $designPath,
                'ini' => $designInfoData
            );
        }
    }

    /**
     * @throws SystemException
     */
    private function searchDefaultDesign()
    {
        $__config = array();
        require(CF_ROOT .'basic.config.php');

        $defaultDesignID = 'default';
        if (isset($this->designs[$__config['defaults']['designID']])) {
            $defaultDesignID = $__config['defaults']['designID'];
        }

        if (!isset($this->designs[$defaultDesignID])) {
            throw new SystemException('There is no default design!');
        }

        $designData = $this->designs[$defaultDesignID];
        $designID = $designData['id'];
        $designDirectory = $designData['path'];
        $designDataFile = $designData['ini'];

        $design = new Design($designID, $designDirectory, $designDataFile);

        $this->setDefaultDesign($design);
    }

    /**
     * @param null $defaultDesign
     */
    public function setDefaultDesign($defaultDesign)
    {
        $this->defaultDesign = $defaultDesign;
    }

    /**
     * @return null|Design
     */
    public function getDefaultDesign()
    {
        return $this->defaultDesign;
    }

    /**
     * @return mixed
     */
    public function getDesignPath()
    {
        return $this->designPath;
    }

}