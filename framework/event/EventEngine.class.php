<?php
namespace SkyFramework\event;

/**
 * Project: SkyFramework2
 * File: EventEngine.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     12.04.2016 - 17:37
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
interface EventEngine
{

    /**
     * @param $name
     * @param $obj
     * @return mixed
     */
    public function fire($name, $obj);

    /**
     * @param $name
     * @param EventListener $listener
     * @return mixed
     */
    public function addListener($name, EventListener $listener);

}