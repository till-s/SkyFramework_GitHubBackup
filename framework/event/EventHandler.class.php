<?php
namespace SkyFramework\event;

use SkyFramework\exception\EventException;
use SkyFramework\singleton\SingletonObject;

/**
 * Project: SkyFramework2
 * File: EventHandler.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     12.04.2016 - 17:33
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class EventHandler extends SingletonObject implements EventEngine
{

    /**
     * @var array
     */
    private $eventListeners = array();

    protected function init()
    {

    }

    /**
     * @param $name
     * @param $obj
     * @return mixed|void
     */
    public function fire($name, $obj)
    {
        if (isset($this->eventListeners[$name])) {
            foreach ($this->eventListeners[$name] as $listener) {
                $listener->execute($obj);
            }
        }
    }

    /**
     * @param $name
     * @param EventListener $listener
     * @return mixed
     */
    public function addListener($name, EventListener $listener)
    {
        if (!isset($this->eventListeners[$name]) || !is_array($this->eventListeners[$name])) {
            $this->eventListeners[$name] = array();
        }

        $this->eventListeners[$name][] = $listener;
    }

}