<?php
namespace SkyFramework\event;

/**
 * Project: SkyFramework2
 * File: EventListener.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     12.04.2016 - 17:36
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
interface EventListener
{

    /**
     * @param $object
     * @return mixed
     */
    public function execute($object);

}