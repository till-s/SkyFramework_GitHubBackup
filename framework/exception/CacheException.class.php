<?php
namespace SkyFramework\exception;

/**
 * Project: SkyFramework2
 * File: CacheException.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     13.04.2016 - 20:35
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class CacheException extends SystemException
{
}