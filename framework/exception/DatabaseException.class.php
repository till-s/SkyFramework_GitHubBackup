<?php
namespace SkyFramework\exception;

/**
 * Project: SkyFramework2
 * File: DatabaseException.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.02.2016 - 10:39
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class DatabaseException extends SystemException
{
}