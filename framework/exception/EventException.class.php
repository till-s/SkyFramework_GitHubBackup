<?php
namespace SkyFramework\exception;

/**
 * Project: SkyFramework2
 * File: EventException.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     12.04.2016 - 17:42
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class EventException extends SystemException
{
}