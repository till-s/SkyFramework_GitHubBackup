<?php
namespace SkyFramework\exception;

/**
 * Project: SkyFramework2
 * File: PrintableException.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.02.2016 - 10:28
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
interface PrintableException
{

    public function show();

}