<?php
namespace SkyFramework\exception;

use SkyFramework\logger\Logger;
use SkyFramework\logger\LoggerEngine;

/**
 * Project: SkyFramework2
 * File: SystemException.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.02.2016 - 10:17
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class SystemException extends AbstractException implements PrintableException
{

    private $className = 'SystemException';

    /**
     * SystemException constructor.
     */
    public function __construct($message)
    {
        parent::__construct($message);

        $reflection = new \ReflectionClass(get_called_class());
        $this->className = $reflection->getShortName();

        Logger::getInstance()->log($this->getMessage() . ' | Code: ' . $this->getCode() . ' | File: ' . $this->getFile() . ':' . $this->getLine(), LoggerEngine::FATAL);
    }

    public function show()
    {
        $output = '<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Fehler aufgetreten!</title>
  </head>
  <body>
    <div class="print-exception">
      <h4 style="font-weigth: bold;">' . $this->className . ': ' . $this->getMessage() . '</h4>
      <table class="print-exception-info">
        <tr>
          <td width="10%"><strong>Message:</strong></td>
          <td>'. $this->getMessage() .'</td>
        </tr>
        <tr>
          <td><strong>File:</strong></td>
          <td>'. $this->getFile() .':'. $this->getLine() .'</td>
        </tr>
        <tr>
          <td><strong>Stacktrace:</strong></td>
          <td>'. str_replace("\n", '<br />', $this->getTraceAsString()) .'</td>
        </tr>
      </table>
    </div>
  </body>
</html>';

        echo $output;
    }

}