<?php
namespace SkyFramework\exception\html;

use SkyFramework\exception\SystemException;

/**
 * Project: SkyFramework2
 * File: HTMLFormException.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 20:04
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class HTMLFormException extends SystemException
{
}