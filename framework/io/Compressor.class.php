<?php
namespace SkyFramework\io;

use SkyFramework\logger\Logger;
use SkyFramework\logger\LoggerEngine;

/**
 * Project: SkyFramework2
 * File: Compressor.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     19.03.2016 - 12:25
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class Compressor
{

    private $files;

    /**
     * Compressor constructor.
     */
    public function __construct($files = array())
    {
        if (!is_array($files)) {
            $files = array($files);
        }

        foreach ($files as $file) {
            if (!file_exists($file)) {
                $files = array_diff($files, array($file));
                Logger::getInstance()->log('No compression of ' . $file . ' for no existence!', LoggerEngine::WARN);
            }
        }

        $this->files = $files;
    }

    public function compress()
    {
        $buffer = "";
        foreach ($this->files as $cssFile) {
            $buffer .= file_get_contents($cssFile);
        }
        $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
        $buffer = str_replace(': ', ':', $buffer);
        $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);

        return $buffer;
    }

}