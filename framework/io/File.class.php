<?php
namespace SkyFramework\io;
use SkyFramework\exception\IOException;

/**
 * Project: SkyFramework2
 * File: File.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     13.02.2016 - 12:25
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class File
{

    protected $filename = "";
    protected $resource = null;

    public function __construct($filename, $mode = 'wb', $options = array()) {
        $this->filename = $filename;
        if (!empty($options)) {
            $context = stream_context_create($options);
            $this->resource = fopen($filename, $mode, false, $context);
        }
        else {
            $this->resource = fopen($filename, $mode);
        }
        if ($this->resource === false) {
            throw new IOException('Resource "'. $filename .'" not found');
        }
    }

    public function __call($function, $arguments) {
        if (function_exists('f' . $function)) {
            array_unshift($arguments, $this->resource);
            return call_user_func_array('f' . $function, $arguments);
        }
        else if (function_exists($function)) {
            array_unshift($arguments, $this->filename);
            return call_user_func_array($function, $arguments);
        }
        else {
            throw new IOException('Function "f'. $function .'" not found');
        }
    }

}