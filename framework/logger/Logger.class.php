<?php
namespace SkyFramework\logger;
use SkyFramework\exception\IOException;
use SkyFramework\io\File;
use SkyFramework\singleton\SingletonObject;

/**
 * Project: SkyFramework2
 * File: Logger.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     15.02.2016 - 16:37
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class Logger extends SingletonObject implements LoggerEngine
{

    private $filePath;
    private $fileHandler;

    private $timeFormat = 'd.m.Y H:i:s';

    /**
     * Logger constructor.
     */
    public function init()
    {
        $this->filePath = ROOT . 'logs/' . $this->getLoggerFilename();
        $this->fileHandler = new File($this->filePath, 'a+');
    }

    public function getLoggerFilename() {
        return date('d_m_y') . '__0_log.txt';
    }

    public function log($message, $logLevel = LoggerEngine::INFO)
    {
        if ($this->fileHandler === null) {
            throw new IOException('File handler for log is null');
        }

        $this->logToFile($message, $logLevel);
    }

    private function logToFile($message, $logLevel) {
        $line = '['. $this->getTime() .'] '. $logLevel .' | '. $message.PHP_EOL;

        $this->fileHandler->lock(LOCK_EX);
        $this->fileHandler->write($line);
        $this->fileHandler->lock(LOCK_UN);
    }

    private function getTime() {
        return date($this->timeFormat);
    }

}