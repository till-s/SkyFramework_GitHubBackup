<?php
namespace SkyFramework\logger;

/**
 * Project: SkyFramework2
 * File: LoggerEngine.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     25.02.2016 - 19:29
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
interface LoggerEngine
{

    const INFO = 'INFO';
    const WARN = 'WARN';
    const ERROR = 'ERROR';
    const FATAL = 'FATAL';

    /**
     * @return string
     */
    public function getLoggerFilename();

    /**
     * @param $message
     * @param string $logLevel
     * @return void
     */
    public function log($message, $logLevel = LoggerEngine::INFO);

}