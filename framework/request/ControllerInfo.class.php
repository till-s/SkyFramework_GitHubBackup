<?php
namespace SkyFramework\request;

/**
 * Project: SkyFramework2
 * File: ControllerInfo.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 16:23
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class ControllerInfo
{

    private $controllerNamespace;
    private $controllerPath;

    /**
     * ControllerInfo constructor.
     * @param $controllerNamespace
     * @param $controllerPath
     */
    public function __construct($controllerNamespace, $controllerPath)
    {
        $this->controllerNamespace = $controllerNamespace;
        $this->controllerPath = $controllerPath;
    }

    /**
     * @return mixed
     */
    public function getControllerNamespace()
    {
        return $this->controllerNamespace;
    }

    /**
     * @return mixed
     */
    public function getControllerPath()
    {
        return $this->controllerPath;
    }

}