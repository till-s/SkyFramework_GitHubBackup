<?php
namespace SkyFramework\request;
use SkyFramework\controller\AbstractController;
use SkyFramework\core\SkyFramework;
use SkyFramework\exception\RouteException;
use SkyFramework\logger\Logger;
use SkyFramework\logger\LoggerEngine;
use SkyFramework\singleton\SingletonObject;
use SkyFramework\util\SessionUtil;
use SkyFramework\util\StringUtil;
use SkyFramework\util\Utils;

/**
 * Project: SkyFramework2
 * File: RequestHandler.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     28.03.2016 - 18:58
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class RequestHandler extends SingletonObject
{

    /**
     * @var null|AltoRouter
     */
    private $routeHandler = null;

    /**
     * @var null|AbstractController
     */
    private $activeRequest = null;

    /**
     * @var null|ControllerInfo
     */
    private $activeControllerInfo = null;

    /**
     * @throws \SkyFramework\exception\SystemException
     */
    protected function init()
    {
        $this->routeHandler = new AltoRouter(array(), '/' . Utils::getURLDirectory(), array());

        $this->routeHandler->map('GET', '', array('controller' => 'Index'), 'home_0');
        $this->routeHandler->map('GET', '/', array('controller' => 'Index'), 'home_1');
        $this->routeHandler->map('GET|POST', '/[a:controller]/[i:id]?/[a:title]?', array(), 'default_0');

        /*if (SessionUtil::isAjaxRequest()) {
            $this->routeHandler->map('POST', '/ajax/[a:controller]', array('path' => 'ajax'), 'ajax_0');
        }*/

        //$this->loadConfigRoutes();
    }

    /**
     * TODO: Deprecated, funktioniert aus irgendeinem Grund nicht mit der Config
     *
     * @throws \SkyFramework\exception\SystemException
     */
    private function loadConfigRoutes()
    {
        $__routes = array();
        //include CF_ROOT . 'routes.config.php';

        foreach ($__routes as $key => $data) {
            //$this->routeHandler->map($data['method'], $data['schema'], $data['target'], $key);
        }
    }

    /**
     * @throws RouteException
     */
    public function handleRequest() {
        $this->buildRequest();
        if ($this->activeRequest !== null) $this->activeRequest->__run();
    }

    private function buildRequest() {
        $match = $this->routeHandler->match();

        if (!$match) {
            throw new RouteException('Invalid route: ""!');
        }

        $params = $match['params'];
        $target = $match['target'];

        $xPath = '';
        $controllerName = null;
        if (!isset($params['controller'])) {
            if (is_array($target)) {
                if (isset($target['controller'])) $controllerName = $target['controller'];
            }
        } else {
            $controllerName = $params['controller'];
        }

        if (is_array($target)) {
            if (isset($target['path'])) $xPath = $target['path'];
        }

        $controllerNamespace = '\\SkyFramework\\controller\\' . (!empty($xPath) ? $xPath . '\\' : '') . strtolower($controllerName) . '\\' . $controllerName;
        $controllerPath = PB_ROOT . 'controller/' . (!empty($xPath) ? $xPath . '/' : '') . strtolower($controllerName) . '/' . $controllerName . '.class.php';

        if (!file_exists($controllerPath)) {
            throw new RouteException('Controller does not exist "' . $controllerPath . '"!');
        }

        if (!class_exists($controllerNamespace)) {
            throw new RouteException('Controller namespace "' . $controllerNamespace . '" does not exist in "' . $controllerPath . '"!');
        }

        $controller = new $controllerNamespace();

        if (!($controller instanceof AbstractController)) {
            throw new RouteException('Controller "' . $controllerNamespace . '" is not an instance of "\SkyFramework\controller\AbstractController"!');
        }

        $this->activeRequest = $controller;
        $this->activeControllerInfo = new ControllerInfo($controllerNamespace, $controllerPath);
    }

    /**
     * @return null|AbstractController
     */
    public function getActiveRequest()
    {
        return $this->activeRequest;
    }

    /**
     * @return null|ControllerInfo
     */
    public function getActiveControllerInfo()
    {
        return $this->activeControllerInfo;
    }

    public function getRoute($controller, $route = 'default_0')
    {
        $params = array('controller' => $controller);

        $domain = Utils::getDomain();
        $routePath = $this->routeHandler->generate($route, $params);
        if (StringUtil::endsWith($domain, '/') && StringUtil::startsWith($routePath, '/')) $domain = substr($domain, 0, -1);

        return $domain . $routePath;
    }

}