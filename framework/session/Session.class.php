<?php
namespace SkyFramework\session;

use SkyFramework\exception\SystemException;
use SkyFramework\request\Request;
use SkyFramework\request\RequestHandler;
use SkyFramework\singleton\SingletonObject;
use SkyFramework\util\SessionUtil;
use SkyFramework\util\StringUtil;

/**
 * Project: SkyFramework2
 * File: Session.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     13.02.2016 - 12:28
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class Session implements SessionEngine
{

    /**
     * Initialize session
     */
    public function __construct()
    {
        $this->startSession();

        if (!isset($_SESSION['sessionID'])) $_SESSION['sessionID'] = StringUtil::getRandomHash();
        if (!isset($_SESSION['request'])) $_SESSION['request'] = null;
        if (!isset($_SESSION['clientIP'])) $_SESSION['clientIP'] = SessionUtil::getUserIPAddress();
        if (!isset($_SESSION['clientAgent'])) $_SESSION['clientAgent'] = SessionUtil::getUserAgent();
        if (!isset($_SESSION['clientLanguage'])) $_SESSION['clientLanguage'] = SessionUtil::getUserLanguage();
        if (!isset($_SESSION['searchBot'])) $_SESSION['searchBot'] = SessionUtil::isSearchBot();
        if (!isset($_SESSION['variables'])) $_SESSION['variables'] = array();
        if (!isset($_SESSION['token'])) $_SESSION['token'] = $this->generateSecurityToken();

        if (!defined('SECURITY_TOKEN')) define('SECURITY_TOKEN', $this->getSecurityToken());
        if (!defined('SECURITY_TOKEN_INPUT_TAG')) define('SECURITY_TOKEN_INPUT_TAG', '<input type="hidden" name="__t" value="' . $this->getSecurityToken() . '" />');
    }

    /**
     * @return void
     */
    public function startSession()
    {
        session_start();
    }

    /**
     * @return void
     */
    public function stopSession()
    {
        session_destroy();
    }

    /**
     * @return null|string
     */
    public function getSessionID()
    {
        return $_SESSION['sessionID'];
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $_SESSION['request'];
    }

    /**
     * @param Request $request
     */
    public function setRequest($request)
    {
        $_SESSION['request'] = $request;
    }

    /**
     * @return null|string
     */
    public function getClientIP()
    {
        return $_SESSION['clientIP'];
    }

    /**
     * @return null|string
     */
    public function getClientAgent()
    {
        return $_SESSION['clientAgent'];
    }

    /**
     * @return null|string
     */
    public function getClientLanguage()
    {
        return $_SESSION['clientLanguage'];
    }

    /**
     * @return boolean
     */
    public function isSearchBot()
    {
        return $_SESSION['searchBot'];
    }

    /**
     * @return int|null
     */
    public function getLastActivity()
    {
        return $_SESSION['lastActivity'];
    }

    /**
     * @return array
     */
    public function getVariables()
    {
        return $_SESSION['variables'];
    }

    /**
     * @param $field
     * @param $value
     * @return mixed|void
     */
    public function setVariable($field, $value)
    {
        if (!is_array($_SESSION['variables'])) $_SESSION['variables'] = array();

        $_SESSION['variables'][$field] = $value;
    }

    /**
     * @param $field
     * @return mixed
     * @throws SystemException
     */
    public function getVariable($field)
    {
        if (!isset($_SESSION['variables'][$field])) {
            throw new SystemException('Session variable "' . $field . '" is not set!');
        }

        return $_SESSION['variables'][$field];
    }

    /**
     * @param $token
     * @return bool
     */
    public function checkSecurityToken($token)
    {
        $hash1 = (string)SECURITY_TOKEN;
        $hash2 = (string)$token;

        if (strlen($hash1) !== strlen($hash2)) {
            return false;
        }

        $result = 0;
        for ($i = 0, $length = strlen($hash1); $i < $length; $i++) {
            $result |= ord($hash1[$i]) ^ ord($hash2[$i]);
        }

        return ($result === 0);
    }

    /**
     * @return null|string|void
     */
    public function getSecurityToken()
    {
        return $_SESSION['token'];
    }

    /**
     * Generate a SecurityToken for AJAX and other things
     */
    private function generateSecurityToken()
    {
        $string = StringUtil::getRandomHash(40);
        $hash = StringUtil::getHash($string . sha1(microtime()) . sha1($this->getSessionID()) . $string);

        return $hash;
    }

    /**
     * @return void
     */
    public function resetSecurityToken()
    {
        unset($_SESSION['token']);
        $_SESSION['token'] = $this->generateSecurityToken();
    }
}