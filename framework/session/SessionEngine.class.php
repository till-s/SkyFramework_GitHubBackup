<?php
namespace SkyFramework\session;

/**
 * Project: SkyFramework2
 * File: SessionEngine.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     25.02.2016 - 19:35
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
interface SessionEngine
{

    /**
     * @return void
     */
    public function startSession();

    /**
     * @return void
     */
    public function stopSession();

    /**
     * @return null|string
     */
    public function getSessionID();

    /**
     * @return Request
     */
    public function getRequest();

    /**
     * @param Request $request
     */
    public function setRequest($request);

    /**
     * @return null|string
     */
    public function getClientIP();

    /**
     * @return null|string
     */
    public function getClientAgent();

    /**
     * @return null|string
     */
    public function getClientLanguage();

    /**
     * @return boolean
     */
    public function isSearchBot();

    /**
     * @return int|null
     */
    public function getLastActivity();

    /**
     * @return array
     */
    public function getVariables();

    /**
     * @param $field
     * @param $value
     * @return mixed
     */
    public function setVariable($field, $value);

    /**
     * @param $field
     * @return mixed
     */
    public function getVariable($field);

    /**
     * @return string
     */
    public function getSecurityToken();

    /**
     * @return void
     */
    public function resetSecurityToken();

}