<?php
namespace SkyFramework\singleton;
use SkyFramework\exception\SystemException;

/**
 * Project: SkyFramework2
 * File: SingletonObject.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     13.02.2016 - 11:49
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class SingletonObject
{

    private static $__instanceObjects = array();

    /**
     * SingletonObject constructor.
     */
    protected final function __construct()
    {
        $this->init();
    }

    protected function init() {

    }

    protected final function __clone() {

    }

    /**
     * @return mixed
     */
    public static final function getInstance() {
        $className = get_called_class();
        if (!array_key_exists($className, self::$__instanceObjects)) {
            self::$__instanceObjects[$className] = null;
            self::$__instanceObjects[$className] = new $className();
        }
        else if (self::$__instanceObjects[$className] === null) {
            throw new SystemException('There is no instance of "'. $className .'" in SingletonObject');
        }

        return self::$__instanceObjects[$className];
    }

}