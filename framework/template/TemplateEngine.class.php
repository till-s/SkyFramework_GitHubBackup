<?php
namespace SkyFramework\template;

/**
 * Project: SkyFramework2
 * File: TemplateEngine.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     25.02.2016 - 16:53
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
interface TemplateEngine
{

    public function setTemplateDir($path);

    public function addTemplateDir($path);

    public function setCompileDir($path);

    public function assign($tpl_var, $value);

    public function display($templateFile);

}