<?php
namespace SkyFramework\util;

use SkyFramework\exception\SystemException;

/**
 * Project: SkyFramework2
 * File: ClassUtil.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     25.02.2016 - 16:59
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class ClassUtil
{

    /**
     * @param $object
     * @param $targetInterface
     * @param null $includePath
     * @return bool
     * @throws SystemException
     */
    public static function isInstanceOf($object, $targetInterface, $includePath = null)
    {
        if ($includePath !== null && file_exists($includePath)) {
            include($includePath);
        }

        if (!class_exists($object)) {
            return false;
            //throw new SystemException('Cannot find class "'. $object .'"');
        } else if (!class_exists($targetInterface) && !interface_exists($targetInterface)) {
            return false;
            //throw new SystemException('Cannot find inheritance interface "'. $targetInterface .'"');
        }

        $class = new \ReflectionClass($object);
        return $class->implementsInterface($targetInterface);
    }

}