<?php
namespace SkyFramework\util;

/**
 * Project: SkyFramework2
 * File: FileUtil.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     24.02.2016 - 17:40
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class FileUtil
{

    /**
     * @param $path
     * @return int
     */
    public static function isValidPath($path)
    {
        return preg_match('#^(\w+/){1,2}\w+\.\w+$#', $path);
    }

    public static function copyRecursive($source, $destination)
    {
        if (is_dir($source)) {
            $dirHandler = opendir($source);

            while ($file = readdir($dirHandler)) {
                if ($file != '.' && $file != '..') {
                    if (is_dir($source . (StringUtil::endsWith($source, '/') ? '' : '/') . $file)) {
                        if (!is_dir($destination . (StringUtil::endsWith($destination, '/') ? '' : '/') . $file)) {
                            mkdir($destination . (StringUtil::endsWith($destination, '/') ? '' : '/') . $file, 0777);
                        }

                        self::copyRecursive($source . (StringUtil::endsWith($source, '/') ? '' : '/') . $file, $destination . (StringUtil::endsWith($destination, '/') ? '' : '/') . $file);
                    } else {
                        copy($source . (StringUtil::endsWith($source, '/') ? '' : '/') . $file, $destination . (StringUtil::endsWith($destination, '/') ? '' : '/') . $file);
                    }
                }
            }

            closedir($dirHandler);
        } else {
            copy($source, $destination);
        }
    }

    public static function getRelativePath($path)
    {
        return substr($path, strlen(ROOT));
    }

}