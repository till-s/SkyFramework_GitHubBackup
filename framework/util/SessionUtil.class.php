<?php
namespace SkyFramework\util;

/**
 * Project: SkyFramework2
 * File: SessionUtil.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     13.02.2016 - 17:05
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
final class SessionUtil
{

    /**
     * @return bool
     */
    public static function isAjaxRequest()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            return (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
        }
        return false;
    }

    /**
     * @return bool
     */
    public static function isSearchBot() {
        return isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider|cloudflare/i', $_SERVER['HTTP_USER_AGENT']);
    }

    /**
     * @return string
     */
    public static function getUserAgent() {
        return substr($_SERVER['HTTP_USER_AGENT'], 0, 255);
    }

    /**
     * @return string
     */
    public static function getUserIPAddress() {
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * @return string
     */
    public static function getUserLanguage() {
        return $_SERVER['HTTP_ACCEPT_LANGUAGE'];
    }

}