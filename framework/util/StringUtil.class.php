<?php
namespace SkyFramework\util;

/**
 * Project: SkyFramework2
 * File: StringUtil.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     13.02.2016 - 12:31
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
final class StringUtil
{

    public static final function getHash($string) {
        return sha1($string);
    }

    public static function getRandomID() {
        return self::getHash(microtime() . uniqid(mt_rand(), true));
    }

    public static function getRandomHash($length = 32) {
        $characters = str_split('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$,??&%/()}][{*~!');
        $randstring = '';
        for ($i = 0; $i < $length; $i++) {
            $randstring .= $characters[mt_rand(0, count($characters) - 1)];
        }
        return self::getHash($randstring);
    }

    public static function quoteString($string) {
        return preg_quote($string);
    }

    public static function startsWith($haystack, $needle) {
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    public static function endsWith($haystack, $needle) {
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }

}