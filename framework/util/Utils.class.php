<?php
namespace SkyFramework\util;

/**
 * Project: SkyFramework2
 * File: Utils.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     26.03.2016 - 16:26
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
final class Utils
{

    public static function getURL()
    {
        $secure = '';
        if (isset($_SERVER['HTTPS'])) {
            $secure = ($_SERVER['HTTPS'] == 'on' ? 's' : '');
        }

        $root = substr(ROOT, strlen($_SERVER['DOCUMENT_ROOT']) + 1);

        return 'http' . $secure . '://' . $_SERVER['SERVER_NAME'] . '/' . $root . (StringUtil::endsWith($root, '/') ? '' : '/');
    }

    public static function getURLPattern()
    {
        $root = substr(ROOT, strlen($_SERVER['DOCUMENT_ROOT']) + 1);
        return 'https?:\/\/' . $_SERVER['SERVER_NAME'] . '\/' . $root . (StringUtil::endsWith($root, '/') ? '' : '\/');
    }

    public static function getURLDirectory()
    {
        $secure = '';
        if (isset($_SERVER['HTTPS'])) {
            $secure = ($_SERVER['HTTPS'] == 'on' ? 's' : '');
        }

        if (defined('DIR')) {
            //substr(ROOT, strlen($_SERVER['DOCUMENT_ROOT']) - 1)

            $dir = substr(DIR, strlen(ROOT));
            $dir = str_replace('\\', '/', $dir);
        } else {
            $dir = substr(getcwd(), strlen(ROOT));
        }

        if (StringUtil::endsWith($dir, '/')) {
            $dir = substr($dir, 0, -1);
        }

        $url = substr(substr(self::getURL(), 0, -1) . ($dir !== false ? '/' . $dir : ''), strlen('http' . $secure . '://' . $_SERVER['SERVER_NAME'] . '/'));

        if (StringUtil::endsWith($url, '/')) {
            $url = substr($url, 0, -1);
        }

        return $url;
    }

    public static function getDomain()
    {
        $secure = '';
        if (isset($_SERVER['HTTPS'])) {
            $secure = ($_SERVER['HTTPS'] == 'on' ? 's' : '');
        }

        $root = substr(ROOT, strlen($_SERVER['DOCUMENT_ROOT']) + 1);

        return 'http' . $secure . '://' . $_SERVER['SERVER_NAME'] . '/';
    }

}