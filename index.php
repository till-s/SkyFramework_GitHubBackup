<?php
/**
 * Project: SkyFramework2
 * File: index.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     29.02.2016 - 20:03
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */

// TODO: http://stackoverflow.com/questions/3080146/post-data-to-a-url-in-php <= Mit AJAX verbinden wenn auf einen Link geklickt wird.

// define main path constant
define('ROOT', dirname(__FILE__) . '/');
define('DIR', dirname(__FILE__) . '/');

require_once(ROOT . 'public/index.php');