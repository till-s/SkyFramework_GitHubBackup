<?php
namespace SkyFramework\controller\ajax;

use SkyFramework\controller\AbstractAJAXController;
use SkyFramework\core\SkyFramework;

/**
 * Project: SkyFramework2
 * File: Test.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     26.03.2016 - 16:00
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class Test extends AbstractAJAXController
{
    public function readParameters()
    {
        $this->sendResponse(array('Erfolgreich!!', SkyFramework::getSession()->getSecurityToken()));
    }

    public function readData()
    {
        // TODO: Implement readData() method.
    }
}