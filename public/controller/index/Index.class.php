<?php
namespace SkyFramework\controller\index;

use SkyFramework\controller\AbstractPageController;
use SkyFramework\controller\index\view\IndexView;
use SkyFramework\core\SkyFramework;

/**
 * Project: SkyFramework2
 * File: Index.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 15:52
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class Index extends AbstractPageController
{

    protected $siteTitle = 'Startseite';

    /**
     * IndexController constructor.
     */
    public function __construct()
    {
        $this->view = new IndexView($this);
    }

}