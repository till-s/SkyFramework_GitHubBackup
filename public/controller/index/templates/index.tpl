{include file="__dochead.tpl"}
{include file="__head.tpl"}

{include file="__header.tpl"}

<!-- Site preloader -->
<section id="preloader">
    <div class="site-spinner"></div>
</section>
<!-- End Site preloader -->

<!-- Intro Section -->
<section id="intro" class="section section-intro">
    <div class="title-wrapper">
        <div class="logo-wrapper">
            <div class="logo"></div>
        </div>
        <div class="headline-wrapper">
            <div class="upper-title-line"></div>
            <h1 class="page-title">{$__pageData.title}</h1>

            <div class="bottom-title-line"></div>
        </div>
    </div>
</section>

<!-- About Section -->
<section id="about" class="section section-about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>About Section</h1>
            </div>
        </div>
    </div>
</section>

<!-- Services Section -->
<section id="services" class="section section-services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Services Section</h1>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section id="contact" class="section section-contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Contact Section</h1>
            </div>
        </div>
    </div>
</section>

{include file="__footer.tpl"}
</body>
</html>