<?php
namespace SkyFramework\controller\index\view;

use SkyFramework\controller\view\AbstractDesignView;

/**
 * Project: SkyFramework2
 * File: IndexView.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 15:53
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class IndexView extends AbstractDesignView
{

    protected $template = 'index.tpl';
    protected $templateDir = 'templates/';

}