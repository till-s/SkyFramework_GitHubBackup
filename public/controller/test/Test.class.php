<?php
namespace SkyFramework\controller\test;

use SkyFramework\controller\test\view\TestView;
use SkyFramework\controller\AbstractPageController;

/**
 * Project: SkyFramework2
 * File: Test.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 16:57
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class Test extends AbstractPageController
{

    protected $siteTitle = 'Testseite';

    /**
     * IndexController constructor.
     */
    public function __construct()
    {
        $this->view = new TestView($this);
    }

}