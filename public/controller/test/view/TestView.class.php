<?php
namespace SkyFramework\controller\test\view;

use SkyFramework\controller\view\AbstractDesignView;

/**
 * Project: SkyFramework2
 * File: TestView.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 16:58
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class TestView extends AbstractDesignView
{

    protected $template = 'test.tpl';
    protected $templateDir = 'templates/';

}