<?php
namespace SkyFramework\controller\testform;

use SkyFramework\controller\testform\view\FormView;
use SkyFramework\controller\AbstractFormController;
use SkyFramework\controller\testform\view\SubmitView;
use SkyFramework\core\SkyFramework;

/**
 * Project: SkyFramework2
 * File: Testform.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 20:12
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class Testform extends AbstractFormController
{

    protected $siteTitle = 'Testformular';

    private $text = '';

    /**
     * IndexController constructor.
     */
    public function __construct()
    {
        $this->view = new FormView($this);
    }

    public function readFormParameters()
    {
        if (isset($_POST['text']) && !empty($_POST['text'])) $this->text = trim(htmlspecialchars($_POST['text']));
    }

    public function assignVariables()
    {
        SkyFramework::getTemplate()->assign('text', $this->text);
    }

    public function finishForm()
    {
        $this->setView(new SubmitView($this));
    }

}