{include file="__dochead.tpl"}
{include file="__head.tpl"}

{include file="__header.tpl"}

<!-- Site preloader -->
<section id="preloader">
    <div class="site-spinner"></div>
</section>
<!-- End Site preloader -->

<div class="container" style="margin-top: 100px;">
    <form action="{$__core->getRequest()->getRoute('Testform')}" method="post" role="form">
        <div class="form-group">
            <label for="inputText">Text:</label>
            <input id="inputText" type="text" class="form-control" name="text"/>
        </div>

        <button type="submit" class="btn btn-primary">Absenden</button>

        {$smarty.const.SECURITY_TOKEN_INPUT_TAG}
    </form>
</div>

{include file="__footer.tpl"}
</body>
</html>