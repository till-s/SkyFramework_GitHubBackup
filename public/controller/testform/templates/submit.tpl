{include file="__dochead.tpl"}
{include file="__head.tpl"}

{include file="__header.tpl"}

<!-- Site preloader -->
<section id="preloader">
    <div class="site-spinner"></div>
</section>
<!-- End Site preloader -->

<div class="container" style="margin-top: 100px;">
    Das Formular wurde erfolgreich abgesendet: {$text}
</div>

{include file="__footer.tpl"}
</body>
</html>