<?php
namespace SkyFramework\controller\testform\view;

use SkyFramework\controller\view\AbstractDesignView;

/**
 * Project: SkyFramework2
 * File: FormView.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 20:13
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class FormView extends AbstractDesignView
{

    protected $template = 'form.tpl';
    protected $templateDir = 'templates/';

}