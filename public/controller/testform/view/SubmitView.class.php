<?php
namespace SkyFramework\controller\testform\view;

use SkyFramework\controller\view\AbstractDesignView;

/**
 * Project: SkyFramework2
 * File: SubmitView.class.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     14.04.2016 - 20:13
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */
class SubmitView extends AbstractDesignView
{

    protected $template = 'submit.tpl';
    protected $templateDir = 'templates/';

}