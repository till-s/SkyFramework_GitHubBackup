<?php
/**
 * Project: SkyFramework2
 * File: index.php
 *
 * @author      Till
 * @copyright   2016 - SkyFramework2 by Till
 * @created     11.02.2016 - 17:46
 * @license     ${LICENSE}
 * @version     ${VERSION}
 */

// define path constants
if (!defined('ROOT')) define('ROOT', dirname(dirname(__FILE__)) . '/');
if (!defined('DIR')) define('DIR', dirname(__FILE__) . '/');

define('FW_ROOT', ROOT .'framework/');
define('CF_ROOT', ROOT .'config/');
define('PB_ROOT', ROOT .'public/');

// initialize SkyFramework-Core
include_once FW_ROOT .'core/SkyFramework.class.php';
new \SkyFramework\core\SkyFramework();

